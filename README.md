# NBAEvolutionVisualizer

__Membres de l'équipe__ : Bourquenoud Nathan, Hirschi Laurent et Pasquier Benjamin

La National Basketball Association a été créée en 1946, sous le nom de Basketball Association of America avant d'être renommée en 1949. Jusqu'à aujourd'hui, cette ligue a évolué dans de nombreux aspects, tant au niveau du jeu que dans son internationalisation. Par exemple, l'arrivée de la ligne à trois points dans les années 80 a changé le jeu à jamais, au point que le tir à trois points demeure aujourd'hui une des armes les plus utilisés dans l'arsenal de tout joueur NBA. Autre exemple, la NBA était principalement, si ce n'est totalement, composé de joueurs américains alors que nous commençons aujourd'hui à voir de plus en plus de joueurs internationaux l'intégrer. Ce projet a donc pour but de visualiser à quel point la NBA a évolué et quels sont les principaux domaines touchés par cette évolution.

## Choix des données

La NBA enregistre un très grand nombre de statistiques durant les matchs disputés mais également toutes sortes d'informations concernant les joueurs et les équipes. Ces données se trouvent sur le [site web officiel](https://www.nba.com/) de la nba et peuvent être accessibles via des APIs. Étant donné qu'il existe des sources de données plus simple à manipuler, cette API n'est pas directement utilisée. Dans le cadre de ce projet, les données sources de données suivantes sont utilisées :
* Un [dataset Kaggle](https://www.kaggle.com/datasets/wyattowalsh/basketball) contenant des données sur les matchs joués depuis 1946 ainsi que sur les joueurs et équipes de la NBA.
* Une [API Python](https://github.com/swar/nba_api) simple d'utilisation permettant d'accéder facilement aux APIs du site officiel de la NBA et donc de toutes les données qu'il contient. Cette API est utilisée pour complétée des données manquantes du dataset Kaggle.

## Technologies utilisées

Ce projet est réalisé en tant qu'application Web, avec une partie frontend pour la visualisation de l'information et une partie backend pour la gestion des données. Les technologies utilisées sont les suivantes :

* __Frontend__ :
    * __Vue.js__ :
    * ?

* Backend :
    * __Python__
    * __FastAPI__
    * __SQLite__

## Intention et public cible

Comme tout sport, le basketball a beaucoup évolué depuis sa création en 1932 et est aujourd'hui un des sports les plus populaires, surtout aux États-Unis. La NBA est la ligue de basketball la plus populaire et prestigieuse du monde et attire aujourd'hui de nombreux joueurs, provenant de divers pays. Depuis sa création en 1946, la NBA n'a cessé de se transformer dans de nombreux aspects. Beaucoup de spécialistes s'accordent par exemple à dire que le jeu était beaucoup plus physique dans les années 1980 à 2000 qu'il ne l'est maintenant. Cependant, la façon de jouer en NBA n'est pas la seule évolution au sein de la ligue, on peut également observer une augmentation de salaire des joueurs (jusqu'à 8 fois supérieur par rapport aux années 1990) ainsi qu'une grande expansion de la ligue vers l'international (29 joueurs internationaux en 1998 contre 108 en 2018).  

L'objectif de ce projet est donc de pouvoir visualiser l'évolution de la NBA selon différents aspects et de confirmer ou non les hypothèses et les avis des spécialistes sur le changement du jeu. De manière générale, la réalisation de ce projet permet à quiconque s'intéressant à la NBA et au basketball en général d'en apprendre plus sur le développement  et l'histoire de cette grande ligue. Elle s'adresse donc autant à des fans de NBA et de basketball qu'aux spécialistes ou aux médias.

## Représentation

### Heat Map des tirs

Montre la localisation des tirs sur un terrain de basket par année.

### Évolution des statistiques

Montre une vue générale sur l'évolution des statistiques principales (nombre de points, de rebons, de contre, ...)

### Évolution des salaires

Possibilité de filtrer selon l'équipe et les joueurs.

### Heat Map d'une carte du monde montrant l'internationalisation de la ligue

Montre le nombre de joueurs par pays, par année.

## Présentation et interaction

## Critique des outils utilisés